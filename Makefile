.PHONY: install

install:
	install -d "${DESTDIR}${PREFIX}/bin"
	install -m 755 powertool "${DESTDIR}${PREFIX}/bin/powertool"
	install -d "${DESTDIR}${PREFIX}/share/man/man1"
	install -m 644 powertool.1 "${DESTDIR}${PREFIX}/share/man/man1/powertool.1"
